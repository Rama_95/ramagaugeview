#
#  Be sure to run `pod spec lint RamaGaugeView.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "RamaGaugeView"
  spec.version      = "1.0"
  spec.summary      = "A short description of RamaGaugeView."

  spec.description  = <<-DESC
    A short description of RamaGaugeView.
                   DESC

  spec.homepage     = "http://rama.com/RamaGaugeView"

  spec.license      = "MIT (Rama)"


  spec.author             = { "@RamaShabaan" => "ramashaaban95@gmail.com" }
 

  spec.platform     = :ios

  spec.ios.deployment_target = "11.0"


  spec.source       = { :git => "git@gitlab.com:Rama_95/RamaGaugeView.git", :tag => "#{spec.version}" }


  spec.source_files  = "Classes/**/*.{h,m,swift}"

end
