//
//  RamaGaugeView.swift
//  RamaGaugeView
//
//  Created by Rama Shaaban on 25/04/2021.
//


import Foundation
import UIKit
import CoreGraphics


public class RamaGaugeView: UIView {
    
    /// in degrees
    @IBInspectable var fullAngle: CGFloat = 0
    @IBInspectable var numberOfLevels: Int = 0
    @IBInspectable var numberOfSections: Int = 0
    @IBInspectable var spaceBetweenSection: CGFloat = 0
    @IBInspectable var title: String = ""
    @IBInspectable var animationDuration: Double = 0
    @IBInspectable var spaceBetweenLevels: CGFloat = 0
    @IBInspectable var imagesOffset: CGFloat = 25
    @IBInspectable var imageDimension: CGFloat = 0
    
    var levelsColors: [UIColor] = []
    
    let pathWidth: CGFloat = 5
    
    let pi: CGFloat =  CGFloat.pi
    
    var r: CGFloat = 0
    
    var centerPoint: CGPoint?
    
    var wholeAngle: CGFloat = 0
    
    var vaildSpace: CGFloat = 0
    
    var arcAngle: CGFloat = 0
    
    var paths = [UIBezierPath]()
    
    var shapeLayers: [CAShapeLayer] = []
    
    var images: [UIImageView] = []
    
	var titleLabel: UILabel?
	
    var data: [GaugeData]  = []
    
    public override var bounds: CGRect {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        self.clipsToBounds = true
    }
    
    public func configure(fullAngle: CGFloat, numberOfLevels: Int, numberOfSections: Int, spaceBetweenSection: CGFloat, spaceBetweenLevels: CGFloat, title: String, animationDuration: Double, levelsColors: [UIColor], data: [GaugeData], imageDimension: CGFloat) {
        
        self.data = data
        
        self.fullAngle = fullAngle
        
        self.numberOfLevels = numberOfLevels
        
        self.numberOfSections = data.count
        
        self.spaceBetweenSection = spaceBetweenSection
        
        self.spaceBetweenLevels = spaceBetweenLevels
        
        self.title = title
        
        self.animationDuration = animationDuration
        
        self.levelsColors = levelsColors
        
        self.imageDimension = imageDimension
        
        self.levelsColors = levelsColors
    }
    
    private func setUpView() {
        
        self.calculateParameters()
        
        self.configureTitleLabel()
        
    }
    
    public override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.setUpView()
        
		self.images.forEach { (imageView) in
			imageView.removeFromSuperview()
		}
		self.images.removeAll()
		
        self.shapeLayers.forEach { (layer) in
            layer.removeAllAnimations()
            layer.removeFromSuperlayer()
        }
        
        for level in 0..<numberOfLevels {
            
            
            let levelRadius = (r + (self.spaceBetweenLevels * CGFloat(level)))
            
            var startAngle : CGFloat = (pi/2) + (2*pi - wholeAngle) / 2
            
            for i in 0..<numberOfSections {
				
				if images.count <= i  {
					let imageView = UIImageView(image: self.data[i].image)
					
					let imagesR = (min(self.bounds.width, self.bounds.height) / 2.0) - (imageDimension / 2.0)
					
					imageView.frame = self.calculteArcCenter(startAngle: startAngle, endAngle: startAngle + arcAngle, radius: imagesR)
					
					self.addSubview(imageView)
					
					self.images.append(imageView)
				}
				
				var activeColorIndex = level
				
				self.images[i].changeImageTintColor(self.levelsColors[0])
                
                if let score = self.data[i].score {
                    
                    activeColorIndex = Int(score) >= self.levelsColors.count ? (self.levelsColors.count - 1) : Int(score)
                    
                }
                
                let path = UIBezierPath(arcCenter: self.centerPoint!, radius: levelRadius, startAngle: startAngle, endAngle: startAngle + arcAngle , clockwise: true)
                
                self.drawArc(path: path, color: self.levelsColors[level].withAlphaComponent(0.2), level: level, index: i, activeColorIndex: activeColorIndex)
                
                startAngle += arcAngle + spaceBetweenSection
                
            }
            
        }
        
    }
    
    func drawArc(path: UIBezierPath, color: UIColor, level: Int, index: Int, activeColorIndex: Int)  {
        
        path.lineWidth = pathWidth
        
        path.lineCapStyle = .butt
        
        color.setStroke()
        
        path.stroke()
        
        let isActive = !(Int(self.data[index].score ?? 0) < level)
        
        if isActive {
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5 + (Double(level) * animationDuration)) {
                
                color.setStroke()
                
                path.stroke()
                
                let shapeLayer = CAShapeLayer()
                
                shapeLayer.lineWidth = self.pathWidth
                
                shapeLayer.path = path.cgPath
                
                shapeLayer.strokeColor = self.levelsColors[activeColorIndex].cgColor
                
                shapeLayer.fillColor = UIColor.clear.cgColor
                
                let basicAnimation = CABasicAnimation(keyPath: #keyPath(UIBezierPath.lineWidth))
                
                basicAnimation.fromValue = 0
                
                basicAnimation.toValue = self.pathWidth
                
                basicAnimation.duration = self.animationDuration
                
                shapeLayer.add(basicAnimation, forKey: "urSoBasic")
                
                self.shapeLayers.append(shapeLayer)
                
                self.layer.addSublayer(shapeLayer)
                
				UIView.animate(withDuration: self.animationDuration, animations: {
					
					self.images[index].changeImageTintColor(color.withAlphaComponent(1))

					
				})
                
            }
            
        }
        
    }
    
    func calculateParameters()  {
        
        self.centerPoint = CGPoint(x: self.bounds.width/2.0, y: self.bounds.height/2.0)
        
        let distanceBetweenLevels: CGFloat = (self.spaceBetweenLevels * CGFloat(numberOfLevels - 1)) + self.imagesOffset
        
        self.r = (min(self.bounds.width, self.bounds.height) / 2) - CGFloat(distanceBetweenLevels) - (self.imageDimension / 2.0)
        
        if self.r <= 0 {
            fatalError("Testtt")
        }
        
        self.wholeAngle = fullAngle * 2 * pi / 360
        
        self.vaildSpace = wholeAngle - (CGFloat(numberOfSections-1) * spaceBetweenSection)
        
        self.arcAngle = vaildSpace / CGFloat(numberOfSections)
    }
    
    func configureTitleLabel()  {
        
		self.titleLabel?.removeFromSuperview()
		
        let label = UILabel(frame: CGRect(x: self.bounds.minX, y: self.bounds.midY + 40, width: self.frame.width, height: 20))
        
        label.textAlignment = .center
        
        label.textColor = .gray
        
        label.text = self.title
        
		self.titleLabel = label
		
        self.addSubview(label)
        
    }
    
    
    func calculteArcCenter(startAngle: CGFloat, endAngle:CGFloat, radius: CGFloat) -> CGRect {
        
        let angle = ((endAngle - startAngle) / 2.0) + startAngle
        
        let arcPointToCircle = CGPoint(x: radius * cos(angle), y: radius * sin(angle))
        
        let archCenter = CGPoint(x: self.centerPoint!.x + arcPointToCircle.x, y: self.centerPoint!.y + arcPointToCircle.y)
        
        return  CGRect(x: archCenter.x - (imageDimension / 2.0), y: archCenter.y - (imageDimension / 2.0), width: imageDimension, height: imageDimension)
        
        
    }
}

