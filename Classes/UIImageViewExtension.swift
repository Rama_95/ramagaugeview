//
//  UIImageViewExtension.swift
//  RamaGaugeView
//
//  Created by Rama Shaaban on 25/04/2021.
//

import Foundation
import UIKit

extension UIImageView {
    
    func changeImageTintColor(_ color: UIColor) -> UIImage? {
        
        let newImage = self.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        
        self.tintColor = color
        
        self.image = newImage
        
        return newImage
        
    }
}
