//
//  GaugeData.swift
//  RamaGaugeView
//
//  Created by Rama Shaaban on 25/04/2021.
//

import Foundation
import  UIKit

public struct GaugeData {
    
    var image: UIImage?
    var score: CGFloat?
    
    public init(image: UIImage, score: CGFloat) {
        
        self.image = image
        self.score = score
    }
}
